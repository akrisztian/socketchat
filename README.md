# SocketChat

## Description
Chat client written in python using kivy. Also comes with a server.

## Usage

- run the server
`python server.py`
- run one or more instances of client
`python client.py`

## Dependencies
- [kivy](https://github.com/kivy/kivy)