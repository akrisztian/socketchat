#Antal Krisztian-Tamas
#521
#akim1784

from kivy.app import App
from kivy.lang import Builder
from kivy.uix.popup import Popup
from kivy.uix.screenmanager import ScreenManager, Screen
from kivy.clock import Clock, mainthread
from kivy.uix.togglebutton import ToggleButton
import kivy.properties as kprop

from functools import partial
import socket
import queue
import threading

HOST = '127.0.0.1'
PORT = 8080

Builder.load_file('ui.kv')

server = None

class ConnectWindow(Screen):
    
    userName = kprop.ObjectProperty(None)

    def connect(self):
        global server
        print(self.userName.text)
        server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server.connect((HOST,PORT))

        command = 'connect!@!{u}'.format(u = self.userName.text)
        server.sendall(command.encode())

        command = server.recv(1024).decode()
        command = command.split('!@!')

        if command[1] == 'OK':
            self.manager.transition.direction = 'up'
            self.manager.current = 'main'
        else:
            pop = ErrorPopup()
            pop.open()
            server.close()


class MainWindow(Screen):

    message = kprop.ObjectProperty(None)
    chat_logs = kprop.ObjectProperty(None)
    stop = threading.Event()

    def __init__(self, **kwargs):
        super(MainWindow,self).__init__(**kwargs)
        self.status = 0
        self.userName = 'Default'
        self.buttons = []
        self.recipient = 'all'

    def change_rec(self, instance):
        self.recipient = instance.text
    
    def on_enter(self):
        self.userName = self.manager.get_screen('connect').ids.userName.text
        self.stop.clear()
        Clock.schedule_once(self.start_threads)

    def start_threads(self, *largs):
        t = threading.Thread(target=self.__read_thread)
        t.setDaemon(True)
        t.start()

    def disconnect(self):
        global server
        self.stop.set()
        server.close()

    def send_message(self):
        print('{u}@{r}: {m}'.format(u = self.userName, r = self.recipient, m = self.message.text))
        command = 'send!@!{s}!@!{r}!@!{m}'.format(s = self.userName, r = self.recipient, m = self.message.text)
        Clock.schedule_once(partial(self.executeCommand,command))
        self.message.text = ''

    @mainthread
    def executeCommand(self, command, *largs):
        global server
        command = command.split('!@!')
        cmd = command[0]

        if cmd == 'send':
            message = 'message!@!{s}!@!{r}!@!{m}'.format(s = command[1], r = command[2], m = command[3])
            server.sendall(message.encode())
            self.message.text = ''
            return

        if cmd == 'update':
            users = command[1]
            users = users.replace('[','')
            users = users.replace(']','')
            users = users.replace("'","")
            users = users.split(', ')

            for button in self.buttons:
                self.ids.user_view.remove_widget(button)
            
            btn = ToggleButton(text = 'all', group='users')
            btn.bind(on_press = self.change_rec)
            btn.value = 'down'
            self.buttons.append(btn)
            self.ids.user_view.add_widget(btn)

            for user in users:
                btn = ToggleButton(text = '{u}'.format(u = user), group='users')
                btn.bind(on_press = self.change_rec)
                self.buttons.append(btn)
                self.ids.user_view.add_widget(btn)
            return

        if cmd == 'message':
            self.chat_logs.text += '[size=20]{s}@{r}: {m}[/size]\n'.format(s = command[1],r = command[2],  m = command[3])
            return

    def __read_thread(self):
        global server
        while True:
                if self.stop.is_set():
                    return
                try:
                    command = server.recv(1024).decode()
                except ConnectionError:
                    return
                Clock.schedule_once(partial(self.executeCommand, command),0)

class ErrorPopup(Popup):
    pass
 
class ClientApp(App):
    def on_stop(self):
        sm.get_screen('main').stop.set()

    def build(self):
        return sm

if __name__ == '__main__':
    sm = ScreenManager()
    sm.add_widget(ConnectWindow(name = 'connect'))
    sm.add_widget(MainWindow(name = 'main'))

    sm.current = 'connect'
    ClientApp().run()


