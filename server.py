#Antal Krisztian-Tamas
#521
#akim1784

import threading
import socket
import sys
import queue

class WebServer(object):

    def __init__(self, port = 8080):
        self.host = ''
        self.port = port
        self.connection = []
        self.commands = []

    def start(self):

        self.socket = socket.socket(socket.AF_INET,socket.SOCK_STREAM)

        try:
            print('Starting server on ' + str(self.host) + ':' + str(self.port))
            self.socket.bind((self.host,self.port))
            print('Server started on port ' + str(self.port))
        except Exception:
            print('Error: Could not bind to port ' + str(self.port))
            self.shutdown()
            sys.exit(1)
        
        self.__listen()

    def shutdown(self):

        try:
            print('Server shutdown')
            self.socket.shutdown(socket.SHUT_RDWR)
        except Exception:
            pass

    #listen for new connections
    def __listen(self):

        self.socket.listen(5)
        index = 0

        while True:
            (client,address) = self.socket.accept()
            print('Received connection from: ' + str(address))
            q = queue.Queue()
            #q.put('update@users')
            self.commands.append(q)
            threading.Thread(target=self.__handle_client, args=(client,address,index,q)).start()
            index += 1
    
    def executeCommand(self, command, client):
        print(command)

        command = command.split('!@!')
        cmd = command[0]
        sender = command[1]

        if cmd == 'connect':
            for conn in self.connection:
                if conn[0] == sender:
                    client.sendall('status!@!NameErr'.encode())
                    client.close()
                    return

            self.connection.append((sender,client))
            client.sendall('status!@!OK'.encode())
            for cmdList in self.commands:
                cmdList.put('update!@!users')
            return

        if cmd == 'message':
            receiver = command[2]
            message = command[3]

            for conn in self.connection:
                if (conn[0] == receiver and conn[1] != client) or (receiver == 'all') or (conn[1] == client):
                    conn[1].sendall('message!@!{s}!@!{r}!@!{m}'.format(s = sender,r = receiver, m = message).encode())
                
            return

        if cmd == 'update':

            users = []
            for conn in self.connection:
                users.append(conn[0])

            client.sendall('update!@!{u}'.format(u=users).encode())


    #handle connected clients
    def __handle_client(self,client,address,index,cmdList):

        PACKET_SIZE = 1024

        #get connect command
        data = client.recv(PACKET_SIZE).decode()
        self.executeCommand(data,client)

        threading.Thread(target= self.__handle_commands, args=(client, address, index, cmdList)).start()

        while True:
            #check for client
            try:
                command = client.recv(PACKET_SIZE).decode()
            except Exception:
                print('{c} disconnected'.format(c = client))
                for conn in self.connection:
                    if conn[1] == client:
                        self.connection.remove(conn)
                self.commands.remove(cmdList)
                print('removed cmdlist')
                for cmdList in self.commands:
                    cmdList.put('update!@!users')
                return

            if not command:
                return

            self.executeCommand(command,client)

    def __handle_commands(self,client,address,index,cmdList):
        while True:
            try:
                command = cmdList.get()

                if command:
                    self.executeCommand(command,client)

                cmdList.task_done()
            except Exception:
                return


           

def main():
    server = WebServer()
    server.start()

if __name__ == '__main__':
    main()